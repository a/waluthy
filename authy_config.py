import os
import json
import base64
import secrets
import hashlib
from getpass import getpass
from Cryptodome.Cipher import AES

config_filename = os.path.join(
    os.path.dirname(os.path.realpath(__file__)), "config.json"
)

_config_cache = None
_crypto_cache = None


def crypto_state():
    global _crypto_cache

    # Load config
    _read_config()

    # Return if _crypto_cache is set or not
    return bool(_crypto_cache)


def init_crypto(password):
    global _crypto_cache

    # Load config
    _read_config()

    # Generate IV, salt and crypto cache
    iv = secrets.token_bytes(16)
    salt = secrets.token_bytes(16)
    _crypto_cache = {"password": password, "iv": iv, "salt": salt}

    # Write config cache (with encryption)
    _write_config_cache()


def deinit_crypto():
    global _crypto_cache

    # Load config
    _read_config()

    # Empty crypto cache
    _crypto_cache = None

    # Write config cache (without encryption)
    _write_config_cache()


def encrypt_json(clear_data):
    global _crypto_cache
    # Dump JSON to string
    clear_data = json.dumps(clear_data)

    # Derive the private key
    priv_key = hashlib.pbkdf2_hmac(
        "sha512",
        _crypto_cache["password"].encode(),
        _crypto_cache["salt"],
        100000,
        dklen=32,
    )

    clear_data = clear_data.encode("utf-8")
    # Modded padding based on https://stackoverflow.com/a/14205319/3286892
    pad_length = 16 - (len(clear_data) % 16)
    clear_data += bytes(pad_length)

    # Encrypt, b64encode and return
    cipher = AES.new(priv_key, AES.MODE_CBC, _crypto_cache["iv"])
    enc_data = cipher.encrypt(clear_data)

    return base64.b64encode(enc_data).decode()


def decrypt_json(enc_data):
    # TODO: maybe combine enc/dec?
    global _crypto_cache
    # Decode the data, salt and IV
    enc_data = base64.b64decode(enc_data)

    # Derive the private key
    priv_key = hashlib.pbkdf2_hmac(
        "sha512",
        _crypto_cache["password"].encode(),
        _crypto_cache["salt"],
        100000,
        dklen=32,
    )

    # Decrypt and return
    cipher = AES.new(priv_key, AES.MODE_CBC, _crypto_cache["iv"])

    clear_data = cipher.decrypt(enc_data).strip(b"\x00").decode()

    return json.loads(clear_data)


def _read_config():
    # Return config from caches if it's available
    global _config_cache
    if _config_cache:
        return _config_cache

    # If config file does not exist, return an empty dict
    if not os.path.exists(config_filename):
        return {}

    with open(config_filename) as f:
        config = json.load(f)

    if "enc_data" in config:
        global _crypto_cache
        password = getpass("Enter your local encryption password: ")

        # Cache crypto details
        salt = base64.b64decode(config["enc_salt"])
        iv = base64.b64decode(config["enc_iv"])
        _crypto_cache = {"password": password, "salt": salt, "iv": iv}

        config = decrypt_json(config["enc_data"])

    # Cache the config and return it
    _config_cache = config
    return config


def _write_config_cache():
    global _config_cache
    global _crypto_cache

    config = _config_cache

    # Encrypt the JSON if we need to
    if _crypto_cache:
        salt = base64.b64encode(_crypto_cache["salt"]).decode()
        iv = base64.b64encode(_crypto_cache["iv"]).decode()
        config = {
            "enc_data": encrypt_json(_config_cache),
            "enc_salt": salt,
            "enc_iv": iv,
        }

    with open(config_filename, "w") as f:
        json.dump(config, f)


def get_value(key, default_val=None):
    config = _read_config()

    # Account for lack of key
    if key not in config:
        return default_val

    return config[key]


def set_value(key, value):
    global _config_cache
    # Account for lack of config, create empty config
    _config_cache = _read_config() if os.path.exists(config_filename) else {}

    # Set value in cache and write cache to disk
    _config_cache[key] = value
    _write_config_cache()

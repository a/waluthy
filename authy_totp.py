import pyotp
import math
import time
import base64

interval = 10


def gen_totp(key):
    totp = pyotp.TOTP(key)
    return totp.now()


def gen_authy_otp(secret):
    # Turn key from base16 to base32, which is what pyotp wants.
    secret = base64.b32encode(base64.b16decode(secret.upper())).decode()
    hotp = pyotp.HOTP(secret, digits=7)

    time_base = time.time()
    step = math.floor(time_base / interval)
    return hotp.at(step)


def convert_secret(secret):
    # Turn key from base16 to base32, which is what pyotp wants.
    secret = base64.b32encode(base64.b16decode(secret.upper())).decode()

    return secret.replace("=", "")


def gen_authy_otps(secret):
    """Takes in Authy device secret (b16 encoded)
    and returns 3 OTP to use as otp1/2/3."""
    # Turn key from base16 to base32, which is what pyotp wants.
    secret = base64.b32encode(base64.b16decode(secret.upper())).decode()
    hotp = pyotp.HOTP(secret, digits=7)

    time_base = time.time()
    base_step = math.floor(time_base / interval)
    return [hotp.at(base_step), hotp.at(base_step + 1), hotp.at(base_step + 2)]

# waluthy

waluthy is an minimal open source client for Authy, one that's WIP but functional.

## Screenshots

![](https://elixi.re/i/jn6nlz42.png)

![](https://elixi.re/i/v28jszww.png)

![](https://elixi.re/i/hzsrst93.png)

## Usage

You need Python 3.7+ to run it. 3.6 may also work, but I'm not sure as I use `required` on argparser which was added on 3.7.

Simply install requirements in requirements.txt (`pip3 install --user -Ur requirements.txt`).

To enroll your account, simply run `python3 authy_cli.py enroll`.

After that, you can get any code by running `python3 authy_cli.py get [name of site]`, partials are also allowed, such as `drop` for `dropbox`.

To sync the latest accounts from your other devices, simply run `python3 authy_cli.py sync`.

That's all!

If you'd like your information to be encrypted, you can use `python3 authy_cli.py crypto`.

If you'd like to migrate your TOTP key to somewhere else, you can use `python3 authy_cli.py getsecret [name of site]` to get the secret/otpauth URI. Same as `get`, partials are also allowed.

One thing when migrating Authy secrets, each account secret is different per device. Revoking Waluthy will also revoke the Authy secrets.

## Features that are missing

- Adding TOTP on PC (and syncing it up)
- Registering on PC
- SMS/Phone authentication
- Not a missing feature, but waluthy devices are displayed as unknown device

## Optional config options

- `force_copy`: bool, default true. If set to true, copies first result on `get` even if `--copy` isn't provided.
- `force_wayland`: bool, default false. If set to true, uses `wl-copy` (of `wl-clipboard`) via subprocess to copy the TOTP code.
- `exit_after_first_result`: bool, default false. If set to true, only shows one result on `get`.
- `show_header`: bool, default true. If set to true, shows `Waluthy - GPLv3 - https://gitlab.com/a/waluthy`.

